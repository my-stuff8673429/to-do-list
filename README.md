# To Do List Application

<b> A simple cross-platoform to-do list app, built using NeutralinoJS. </b>

![Video](Video/video.gif)

## Summary 

This app takes a new spin on the basic to do list format, where you can seperate what needs to be doe, is being worked on, and is completed in one simple program.

## How to use

Using this app, users are able to create tasks that they want to acomplish. 

They can move a task into the ```Doing``` column to indicate that they are working on the task. 

Once they are finished they can place the task into the ```Done``` column to indicate completion of the task. 

If the user wishes to delete a task from the list, they can click the ```Delete``` button to free up a row. 

When they close the app, it will automatically save their progress.

This app features two themes to choose from: dark mode and light mode. Users can switch which theme they want by pressing the ```Change Theme``` Button.


## Installation

### Option A (Prebuilt binaires):
1. Download the zip file from the ```releases``` tab.
2. In the zip file, find the folder that coresponds to your operating system.
3. Run the executable in the folder.

### Opton B (Compile from source):
1. Ensure NeutralinoJS is installed on your device, if not follow the instaltion steps on their website: https://neutralino.js.org/.
2. Download the zip file from the releases tab.
3. Find the ```Source``` folder in the zip file.
4. To run the program type ```neu run``` or to build the program type ```neu build```.

## Screenshots
### Dark Theme 
![Dark Theme](Screenshots/Dark_Theme.png)

### Light Theme 
![Light Theme](Screenshots/Light_Theme.png)

### Credits

This project was created using HTML, CSS, and JS. Using NeutralinoJS to distrubte as an application.

This project will always be 100% free and open source.
