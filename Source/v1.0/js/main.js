// https://gitlab.com/CatBoi110
// V1.0

Neutralino.init();

Neutralino.events.on("ready",() => {
    loadList();
    loadSavedTheme();
});

Neutralino.events.on("windowClose", () => {
    writeList();
    writeSavedTheme();

    Neutralino.app.exit();
});

let table = document.querySelector("#Main-Table");

let newButton = document.querySelector("#New-Button");
let newTaskInput = document.querySelector("#New-Input");
let acceptButton = document.querySelector("#Accept-Button");
let cancelButton = document.querySelector("#Cancel-Button");
let rightArrow = document.querySelector("#Right-Arrow");
let leftArrow = document.querySelector("#Left-Arrow");
let deleteButton = document.querySelector("#Delete-Button");
let revertButton = document.querySelector("#Revert-Table");
let changeThemeButton = document.querySelector("#Change-Theme");

let selectedRow = null;
let selectedCell = null;

newButton.addEventListener("click", newTaskPrompt);
acceptButton.addEventListener("click", addTask);
cancelButton.addEventListener("click", cancelTask);
deleteButton.addEventListener("click", deleteTask);
revertButton.addEventListener("click", revertList);
changeThemeButton.addEventListener("click", changeTheme);
leftArrow.addEventListener("click", function () { moveTask("left")});
rightArrow.addEventListener("click", function () { moveTask("right")});

disableElement(deleteButton);
disableElement(revertButton);
disableElement(leftArrow);
disableElement(rightArrow);

function showElement(element){
    element.classList.remove("Hidden");
    element.classList.add("Shown");
}

function hideElement(element){
    element.classList.remove("Shown");
    element.classList.add("Hidden");
}

function disableElement(element){
    element.style.color = "rgba(0, 0, 0, 0.5)";
    element.disabled = true;
}

function enableElement(element){
    element.style.color = "";
    element.disabled = false;
}


function newTaskPrompt(){
    showElement(newTaskInput);
    showElement(acceptButton);
    showElement(cancelButton);

    hideElement(newButton);
}

function addTask(){
    hideElement(newTaskInput);
    hideElement(acceptButton);
    hideElement(cancelButton);

    showElement(newButton)

    if (!newTaskInput.value.includes(",")){
        let task = newTaskInput.value;
        newTaskInput.value = "";


        for (let i = 0; i < table.rows.length; i++){
            if (table.rows[i].cells[0].innerHTML === "" && table.rows[i].cells[1].innerHTML === "" && table.rows[i].cells[2].innerHTML === ""){
                table.rows[i].cells[0].innerHTML = task;
                break;
            }
        }

        enableElement(revertButton);
    } else {
        alert("Please enter a task without a comma.");

        newTaskInput.value = "";
    }
}

function cancelTask(){
    hideElement(newTaskInput);
    hideElement(acceptButton);
    hideElement(cancelButton);

    showElement(newButton);

    newTaskInput.value = "";
}

function selectTask(element){
    selectedRow = element.closest("tr").rowIndex;
    selectedCell = element.cellIndex;

    if (element.innerHTML !== ""){
        isSelected = true;

        switch (selectedCell){
            case 0:
                disableElement(leftArrow);
                enableElement(deleteButton);
                enableElement(rightArrow);
    
                break;
    
            case 1:
                enableElement(leftArrow);
                disableElement(deleteButton);
                enableElement(rightArrow);

                break;
    
            case 2:
                disableElement(leftArrow);
                enableElement(deleteButton);
                disableElement(rightArrow);

                break;
        }

    }
}

function moveTask(direction){
    let currentCell = table.rows[selectedRow].cells[selectedCell];
    let nextCell = null;

    if (direction === "left"){
        nextCell = table.rows[selectedRow].cells[selectedCell - 1];
    } else if (direction === "right"){
        nextCell = table.rows[selectedRow].cells[selectedCell + 1];
    }


    isSelected = false;

    nextCell.innerHTML = currentCell.innerHTML;
    
    currentCell.innerHTML = "";
    currentCell.style.background = "";

    disableElement(leftArrow);
    disableElement(deleteButton);
    disableElement(rightArrow);

    enableElement(revertButton);
}

function deleteTask(){
    let currentRow = table.rows[selectedRow];

    for (let i = 0; i < currentRow.cells.length; i ++){
        currentRow.cells[i].innerHTML = "";
    }

    isSelected = false;

    currentRow.cells[selectedCell].style.background = "";

    disableElement(leftArrow);
    disableElement(deleteButton);
    disableElement(rightArrow);

    enableElement(revertButton);
}

async function writeList(){
    let allRows = [];
    let currentRow = [];

    for (let i = 1; i < table.rows.length; i ++){
        for (let j = 0; j < table.rows[i].cells.length; j ++){
            currentRow.push(table.rows[i].cells[j].innerHTML);
        }

        currentRow.push();
        allRows.push(currentRow);
        currentRow = [];
    }

    let statement = "";

    for (let i = 0; i < allRows.length; i ++){
        statement += allRows[i].toString() + "\n";
    }

    try{
        await Neutralino.filesystem.writeFile("savedList.txt", statement);
    } catch (err){
        console.log("Err" + err);
    }
}

async function loadList(){
    let savedRows = [];

    try{
        savedRows = await Neutralino.filesystem.readFile("savedList.txt");
    } catch (err){
        console.log("Err" + err);
    }

    savedRows = savedRows.split(["\n"]);

    let allRows = [];
    let currentRow = [];

    for (let i = 0; i < savedRows.length; i++){
        for (let j = 0; j < savedRows[i].length; j++){
            currentRow.push(savedRows[i].split(",")[j]);
        }

        currentRow.push();
        allRows.push(currentRow);
        currentRow = [];


    }

    for (let i = 1; i < table.rows.length; i ++){
        for (let j = 0; j < table.rows[i].cells.length; j ++){
            if (allRows[i - 1][j] != undefined){
                table.rows[i].cells[j].innerHTML = allRows[i - 1][j];
            } else {
                table.rows[i].cells[j].innerHTML = "";
            }
        }
    }
}

function revertList(){
    loadList();

    selectedRow = null;
    selectedCell = null;

    disableElement(leftArrow);
    disableElement(deleteButton);
    disableElement(rightArrow);
    disableElement(revertButton);
}

function changeTheme(){
    if (document.documentElement.classList[0] === "Dark-Theme"){
        document.documentElement.classList.remove("Dark-Theme");
        document.documentElement.classList.add("Light-Theme");

    } else if (document.documentElement.classList[0] === "Light-Theme"){
        document.documentElement.classList.add("Dark-Theme");
        document.documentElement.classList.remove("Light-Theme");
    }
}

async function writeSavedTheme(){
    try{
        await Neutralino.filesystem.writeFile("savedTheme.txt", document.documentElement.classList[0]);
    } catch (err) {
        console.log("Err:" + err);
    }
}

async function loadSavedTheme(){
    try{
        let savedTheme = await Neutralino.filesystem.readFile("savedTheme.txt");
        document.documentElement.classList.add(savedTheme.toString().trim());
    } catch (err){
        document.documentElement.classList.add("Dark-Theme");
    }
}